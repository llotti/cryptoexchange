const Btoken = artifacts.require('./Btoken.sol');

contract('B token', (accounts) => {
  let btoken;

  beforeEach(async() => {
    btoken = await Btoken.deployed();
  });

  it('deploys the contract with total supply', async() => {
    let totalSupply = await btoken.totalSupply();
    assert.equal(totalSupply, '1000000');
  });

  it('has an owner', async() => {
    let owner = await btoken.owner();
    assert.equal(owner, accounts[0]);
  });

  it('assigns total supply to owner upon deployment', async() => {
    let owner = await btoken.owner();
    let balance = await btoken.balanceOf(owner);
    assert.equal(balance, '1000000');
  });

  it('transfers tokens correctly', async() => {
    await btoken.transfer(accounts[1], 10, { from: accounts[0]});
    let balance = await btoken.balanceOf(accounts[1]);
    assert.equal(balance, '10');
  });

  it('requires minimum amount of tokens', async() => {
    try {
      await btoken.transfer(accounts[3], 1000, { from: accounts[1]});
      assert(false);
    } catch (err) {
      assert(err);
    }
  });

  it('approves allowance for delegated transfer', async() => {
    await btoken.approve(accounts[1], 100, { from: accounts[0]});
    let allowance = await btoken.allowance(accounts[0], accounts[1]);
    assert.equal(allowance, 100);
  });

  it('handles delegated transfer correctly', async() => {
    const fromAccount = accounts[0];
    const toAccount = accounts[2];
    const delegatedAccount = accounts[1];

    await btoken.approve(delegatedAccount, 100, { from: fromAccount });
    await btoken.transferFrom(fromAccount, toAccount, 50, { from: delegatedAccount });
    let remaining = await btoken.allowance(fromAccount, delegatedAccount);
    assert.equal(remaining, 50);
  });

  it('requires minimum allowance for delegated transfer', async() => {
    const fromAccount = accounts[0];
    const toAccount = accounts[2];
    const delegatedAccount = accounts[1];

    await btoken.approve(delegatedAccount, 100, { from: fromAccount });

    try {
      await btoken.transferFrom(fromAccount, toAccount, 200, { from: delegatedAccount });
      assert(false);

    } catch (err) {
      assert(err);
    }
  })
})













