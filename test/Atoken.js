const Atoken = artifacts.require('./Atoken.sol');

contract('A token', (accounts) => {
  let atoken;

  beforeEach(async() => {
    atoken = await Atoken.deployed();
  });

  it('deploys the contract with total supply', async() => {
    let totalSupply = await atoken.totalSupply();
    assert.equal(totalSupply, '1000000');
  });

  it('has an owner', async() => {
    let owner = await atoken.owner();
    assert.equal(owner, accounts[0]);
  });

  it('assigns total supply to owner upon deployment', async() => {
    let owner = await atoken.owner();
    let balance = await atoken.balanceOf(owner);
    assert.equal(balance, '1000000');
  });

  it('transfers tokens correctly', async() => {
    await atoken.transfer(accounts[1], 10, { from: accounts[0]});
    let balance = await atoken.balanceOf(accounts[1]);
    assert.equal(balance, '10');
  });

  it('requires minimum amount of tokens', async() => {
    try {
      await atoken.transfer(accounts[3], 1000, { from: accounts[1]});
      assert(false);
    } catch (err) {
      assert(err);
    }
  });

  it('approves allowance for delegated transfer', async() => {
    await atoken.approve(accounts[1], 100, { from: accounts[0]});
    let allowance = await atoken.allowance(accounts[0], accounts[1]);
    assert.equal(allowance, 100);
  });

  it('handles delegated transfer correctly', async() => {
    const fromAccount = accounts[0];
    const toAccount = accounts[2];
    const delegatedAccount = accounts[1];

    await atoken.approve(delegatedAccount, 100, { from: fromAccount });
    await atoken.transferFrom(fromAccount, toAccount, 50, { from: delegatedAccount });
    let remaining = await atoken.allowance(fromAccount, delegatedAccount);
    assert.equal(remaining, 50);
  });

  it('requires minimum allowance for delegated transfer', async() => {
    const fromAccount = accounts[0];
    const toAccount = accounts[2];
    const delegatedAccount = accounts[1];

    await atoken.approve(delegatedAccount, 100, { from: fromAccount });

    try {
      await atoken.transferFrom(fromAccount, toAccount, 200, { from: delegatedAccount });
      assert(false);

    } catch (err) {
      assert(err);
    }
  })
})













