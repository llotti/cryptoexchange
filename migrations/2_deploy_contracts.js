var Atoken = artifacts.require("./Atoken.sol");
var Btoken = artifacts.require("./Btoken.sol");

module.exports = function(deployer) {
  deployer.deploy(Atoken);
  deployer.deploy(Btoken);
};
